<?php

namespace App\Http\Controllers;

use App\Models\taka;
use Illuminate\Http\Request;

class TakaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=taka::all();
        return view('jadwal', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Taka::create([
            'hari'=>$request->hari,
            'matkul'=>$request->matkul,
            'jam'=>$request->jam,
            'dosen'=>$request->dosen,
        ]);

        return redirect('jadwal');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\taka  $taka
     * @return \Illuminate\Http\Response
     */
    public function show(Taka $id)
    {
        $satudata = taka::find($id);
        return view('ditel', compact('satudata'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\taka  $taka
     * @return \Illuminate\Http\Response
     */
    public function edit(taka $taka, $id)
    {
        $edit = taka::find($id);
        return view('edit', compact('edit'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\taka  $taka
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $edit = taka::findorfail($id);
        $edit->update($request->all());

        return redirect('jadwal');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\taka  $taka
     * @return \Illuminate\Http\Response
     */
    public function destroy(taka $taka, $id)
    {
        $edit = taka::findorfail($id);
        $edit->delete();

        return back();
    }
}
