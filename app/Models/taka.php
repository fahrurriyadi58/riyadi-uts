<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class taka extends Model
{
    use HasFactory;
    protected $fillable = [
        'hari', 'matkul', 'jam', 'dosen',
    ];
}
