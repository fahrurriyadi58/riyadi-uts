<?php

use App\Http\Controllers\TakaController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('conten');
});

Route::get('jadwal',[TakaController::class, 'index']);
Route::get('jadwal/{id}', [TakaController::class, 'show']);
Route::get('buat', [TakaController::class, 'create']);
Route::post('simpan', [TakaController::class, 'store']);
Route::get('edit/{id}', [TakaController::class, 'edit']);
Route::post('update/{id}', [TakaController::class, 'update']);
Route::get('hapus/{id}', [TakaController::class, 'destroy']);