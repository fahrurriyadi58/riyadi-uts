<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Taka;

class TakaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Taka::create([
            'hari' => 'Senin',
            'matkul' => 'Pemrog. Web',
            'jam' => '4-5',
            'dosen' => 'Rofi Ariev',
        ]);
    }
}
